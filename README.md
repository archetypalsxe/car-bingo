# Car Bingo

Mostly a learning application to work with SST, Apollo, React, and Typescript

## Setup/Installation
* In the `car-bingo` directory:
  * Copy the `.env` file to `.env.local` and make required changes
    * Changes to `.env.local` will overwrite the values in `.env` and won't be tracked by git
  * `npm install`
* And then in the `car-bingo/frontend` directory:
  * `npm install`
    * (again)

## Running "Locally"

### API
* From the `car-bingo` directory: `npm start`
  * This does require AWS credentials to be setup
  * It basically uses a live Lambda development environment
  * A noticeable difference between SST and serverless, is that SST is able to use my default `aws-vault` setup, while I wasn't able to with serverless

### FrontEnd
* From the `car-bingo/frontend` directory:
  * `npm start`

### Accessing

#### SST Console
* If you navigate to `console.sst.dev` the SST console should be available
  * Note: If you're using Brave, you do have to lower the Brave shields

#### API
* Apollo can be accessed directly through the provided API Gateway link
* Can also be accessed through the SST console: `https://console.serverless-stack.com/`
  * If using Brave, may have to turn off Shields to access
  * GraphQL should be an available tab to access Apollo as well

#### FrontEnd
* A browser window should open automatically after the `npm start`, but if it doesn't...
  * It's generally available at `localhost:3000`, or it will prompt you to use a different port if that port is already in use

#### Example Queries:
```
query {
  listBoards {
    id,
    playerId,
    boxes,
    marked
  }
}
```
```
query {
  listItems {
    id,
    name,
    imageUrl
  }
}
```
```
mutation {
  createBoard(
  )
}
```

#### From Apollo Studio:
Operation:
```
mutation CreateBoard($board: CreateBoardInput!) {
  createBoard(board: $board) {
    id,
    playerId,
    boxes,
    marked
  }
}
```
Variable:
```
{
  "board": {
    "id": "2",
    "playerId": "2",
    "boxes": "{xyz}",
    "marked": "{x}"
  }
}
```

### Modifying
* If you have `npm start` running, just modify the source files and it seems to automagically get deployed
  * Assumed that it wasn't working because there wasn't any output, but confirmed that the page does get uploaded

### Removing
* `npm run remove`
  * Defaults to `local`

## Deploying to AWS
* `npx sst deploy --stage dev`
  * Defaults to `dev`
  * There is a check for a stage named `prod` for production

### Removing
* `npx sst remove --stage dev`

## Commands Used to Setup This Repository
Shouldn't have to be ran, but may be useful

* Creating the SST app:
  * `npm init sst typescript-starter car-bingo`
    * When I tried to run this with an older version of Node/npm I received an error about not being able to find the `fs` module
      * I upgrade to Node version `16.15.1` and npm version `8.11.0` and this seemed to resolve the issue
* Creating React App:
  * `npx create-react-app frontend --use-rpm`

## Terraform
Currently, Terraform is just used to setup Datadog modules and monitoring

### PreRequirements
* Terragrunt is required
* Assumes that you have my [awsAccountIntegration module](https://gitlab.com/archetypalsxe/datadog-terraform-resources/-/tree/main/modules/awsAccountIntegration) deployed and that the `keys_to_create` parameter includes the project_name used for this project
  * If you do not...
    * Create a secret with the name `datadog-keys-${var.project_name}`
    * It should have two keys, `api_key` and `app_key`, which should be set to the appropriate Datadog keys accordingly

### Setup
* From the `terraform/datadog` directory:
  * Setup Terragrunt Configuration:
    * Copy the file `terragrunt-config-example.yaml` to `terragrunt-config.yaml`
    * Modify the file according to needs
  * Use terragrunt to setup the state bucket/table:
    * `terragrunt init`

### Usage
* `terragrunt plan`
  * See pending changes
* `terragrunt apply`
  * Apply pending changes

## References / Inspiration
* [Original Skeleton Repository](https://gitlab.com/archetypalsxe/sst-apollo)
* [Setting Up API/Database/Front End in SST](https://serverless-stack.com/examples/how-to-create-a-reactjs-app-with-serverless.html)
* [Example of Apollo Connecting to DynamoDB](https://github.com/Wkasel/cdk-apollo-dynamo-serverless-stack)
* [Apollo Docs for Mutations/Local Cache](https://www.apollographql.com/docs/react/data/mutations/#updating-local-data)
* [Setting Up Cognito Identity Pools with SST](https://docs.sst.dev/api#cognito-identity-pool)
* [Example Using Auth Through Cognito](https://github.com/serverless-stack/sst/tree/master/examples/react-app-auth-cognito)
* [Question on Stack Overflow Regarding Apollo and AWS Amplify's Auth](https://stackoverflow.com/questions/69900718/sending-amplify-access-token-to-server-via-apollo-client)
* [AWS Amplify Auth Docs](https://aws-amplify.github.io/amplify-js/api/classes/authclass.html)
* [CDK Docs for Cognito User Pool](https://docs.aws.amazon.com/cdk/api/v2/docs/aws-cdk-lib.aws_cognito.UserPoolProps.html)
* [Example of Copying Text to Clipboard](https://www.tutsmake.com/react-copy-text-to-clipboard-example/)
* [GitLab CI/CD Pipeline Article](https://dev.to/6thcode/how-to-set-up-a-cicd-environment-on-gitlab-using-nodejs-jh3)