import { gql  } from "apollo-server-lambda";

export const typeDefs = gql`
  type Board {
    id: ID!
    playerId: String!,
    boxes: String!,
    marked: String
  }

  type Item {
    id: ID!
    name: String!
    imageUrl: String
  }

  input CreateBoardInput {
    playerId: String!,
    boxes: String!,
    marked: String
  }

  type Query {
    hello: String
    getBoardByPlayerId(playerId:String): Board
    getItem(itemId:String): Item
  }

  type Mutation {
    createBoard(board:CreateBoardInput!):Board,
    deleteBoard(boardId:String!):String,
    generateNewBoard(playerId:String):Board,
    updateBoardMarked(boardId:String!, markedBoxes:String!):Board,
  }
`;