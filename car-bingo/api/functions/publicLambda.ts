import { ApolloServer } from "apollo-server-lambda";
const { merge } = require('lodash');

import { typeDefs as publicTypeDefs} from "./publicTypeDefs"
import { public_resolvers  } from "./publicResolvers"


const typeDefs = `
  type Query {
    _empty: String
  }
  type Mutation {
    _empty: String
  }
`;

const resolvers = {
    Query: {
    },
    Mutation:{
    },
};

// https://mernapps.com/seperate-typedefs-and-resolvers-in-to-files-graphql-apollo-server/
const server = new ApolloServer({
  typeDefs:[publicTypeDefs],
  resolvers:merge(resolvers,public_resolvers),
  introspection: !!process.env.IS_LOCAL,
  context: ({ event, context, express }) => ({
    headers: event.headers,
    functionName: context.functionName,
    event,
    context,
    expressRequest: express.req,
  }),
});

export const handler = server.createHandler();