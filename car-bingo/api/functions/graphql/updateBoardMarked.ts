import Board from "./BoardTable";
import updateBoard from "./updateBoard";

export default async function updateBoardMarked(boardId: string, markedBoxes: string): Promise<Board> {
  const board = {
    id: boardId,
    marked: markedBoxes
  }

  return await updateBoard(board) as Board;
}