import { DynamoDB } from "aws-sdk";
import Item from "./ItemTable";

const dynamoDb = new DynamoDB.DocumentClient();

/*
query GetItem {
  getItem (itemId: "3") {
    id
    name
    imageUrl
  }
}
*/

/* Or... Operation
query GetItem($itemId: String) {
  getItem(itemId: $itemId) {
    id
    name
    imageUrl
  }
}
*/

/* Variables
{
  "itemId": "2"
}
*/

export default async function getItem(
    itemId: String
): Promise<Item | undefined> {
    const params = {
        TableName: process.env.ITEMS_TABLE as string,
        Key: { id: itemId }, 
    };

    const data = await dynamoDb.get(params).promise();

    if ( data.Item ) {
      return data.Item as Item;
    }

    return undefined;
}