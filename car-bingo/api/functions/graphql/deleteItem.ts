import { DynamoDB } from "aws-sdk";

const dynamoDb = new DynamoDB.DocumentClient();

export default async function deleteItem(id: string): Promise<string> {
    const params = {
        Key: { id: id },
        TableName: process.env.ITEMS_TABLE as string,
    };

    await dynamoDb.delete(params).promise();

    return id;
}