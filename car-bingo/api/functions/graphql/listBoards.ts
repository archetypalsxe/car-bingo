import { DynamoDB } from "aws-sdk";

const dynamoDb = new DynamoDB.DocumentClient();

export default async function listBoards(): Promise<
    Record<string, unknown>[] | undefined
> {
    console.log("BOARD TABLE PARAM", process.env.BOARD_TABLE)
    const params = {
        TableName: process.env.BOARD_TABLE as string,
    };

    const data = await dynamoDb.scan(params).promise();

    return data.Items;
}