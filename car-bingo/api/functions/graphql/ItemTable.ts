type Item = {
  id?: string;
  name: string;
  imageUrl: string;
};

export default Item;