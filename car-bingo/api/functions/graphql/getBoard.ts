import { DynamoDB } from "aws-sdk";
import Board from "./BoardTable";

const dynamoDb = new DynamoDB.DocumentClient();

/*
query GetBoard {
  getBoard (boardId: "3") {
    id
    playerId
    boxes
    marked
  }
}
*/

/* Or... Operation
query GetBoard($boardId: String) {
  getBoard(boardId: $boardId) {
    id
    playerId
    boxes
    marked
  }
}
*/

/* Variables
{
  "boardId": "3"
}
*/

export default async function getBoard(
    boardId: String
): Promise<Board | undefined> {
    const params = {
        TableName: process.env.BOARD_TABLE as string,
        Key: { id: boardId },
    };

    const data = await dynamoDb.get(params).promise();

    if ( data.Item ) {
      return data.Item as Board;
    }

    return undefined;
}