import { DynamoDB } from "aws-sdk";
import Item from "./ItemTable";
import listItems from "./listItems";

const dynamoDb = new DynamoDB.DocumentClient();

export default async function createBoard(item: Item): Promise<Item> {

    const existingItems = await listItems();
    if (existingItems) {
        item.id = String(existingItems.length + 1);
    } else {
        item.id = "1";
    }

    console.log(`Creating an item.... ${JSON.stringify(item)}`);

    const params = {
        Item: item as Record<string, unknown>,
        TableName: process.env.ITEMS_TABLE as string,
    };

    await dynamoDb.put(params).promise();

    return item;
}