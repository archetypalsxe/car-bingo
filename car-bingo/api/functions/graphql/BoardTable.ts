type Board = {
  id?: string;
  timeToLive?: number;
  playerId?: string;
  boxes?: string;
  marked?: string;
};

export default Board;