import { DynamoDB } from "aws-sdk";
import createItem from "./createItem";
import Item from "./ItemTable";
import listItems from "./listItems";

const dynamoDb = new DynamoDB.DocumentClient();

/*
mutation Mutation {
  populateTables
}
*/

export default async function populateTables(): Promise<String> {

    let items = 0;

    const existing_items = await listItems();
    if (existing_items && existing_items.length < 1) {
      items = await populate_items();
    }

    return `Inserted ${items} item(s)`;
}

async function populate_items(): Promise<number> {
  console.log(`Populating items...`);

  let created = 0;

  const itemsToAdd:Item[] = new Array(
    {
      name: "Cow",
      imageUrl: "cow.jpg",
    },
    {
      name: "Kayak",
      imageUrl: "kayak.jpg",
    },
    {
      name: "Bicycle",
      imageUrl: "bike.jpg",
    },
    {
      name: "Police",
      imageUrl: "popo.png",
    },
    {
      name: "Camper / RV",
      imageUrl: "rv.jpeg",
    },
    {
      name: "Yellow Vehicle",
      imageUrl: "yellow.png",
    },
    {
      name: "Horse",
      imageUrl: "horse.jpg",
    },
    {
      name: "Stopped Vehicle",
      imageUrl: "broken_car.jpg",
    },
    {
      name: "Roof Luggage",
      imageUrl: "roof_luggage.jpg",
    },
    {
      name: "Electric Car",
      imageUrl: "electric-car.jpg",
    },
    {
      name: "Electric Car Charge",
      imageUrl: "car-charger.jpg",
    },
    {
      name: "Truck",
      imageUrl: "truck.jpeg",
    },
    {
      name: "Rest Stop",
      imageUrl: "rest-area.jpg",
    },
    {
      name: "Isolated Tree",
      imageUrl: "isolated-tree.jpg",
    },
    {
      name: "Dead Tree",
      imageUrl: "dead-tree.jpg",
    },
    {
      name: "Foreign License Plate",
      imageUrl: "license-plates.jpg",
    },
    {
      name: "Motorcycle",
      imageUrl: "motorcycle.jpg",
    },
    {
      name: "Plane",
      imageUrl: "plane.jpg",
    },
    {
      name: "Bird",
      imageUrl: "bird.jpg",
    },
    {
      name: "Animal Crossing",
      imageUrl: "animal-crossing.gif",
    },
    {
      name: "Barn",
      imageUrl: "barn.jpg",
    },
    {
      name: "Flower",
      imageUrl: "flowers.jpg",
    },
  )

  for (const itemToAdd of itemsToAdd) {
    if(await createItem(itemToAdd)) {
      created++;
    }
  }

  return created;
}