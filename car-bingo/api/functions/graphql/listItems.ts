import { DynamoDB } from "aws-sdk";

const dynamoDb = new DynamoDB.DocumentClient();

export default async function listItems(): Promise<
    Record<string, unknown>[] | undefined
> {
    console.log("ITEMS TABLE PARAM", process.env.ITEMS_TABLE)
    const params = {
        TableName: process.env.ITEMS_TABLE as string,
    };

    const data = await dynamoDb.scan(params).promise();

    return data.Items;
}