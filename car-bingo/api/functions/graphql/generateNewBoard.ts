import Board from "./BoardTable";
import Item from "./ItemTable";

import createBoard from "./createBoard";
import getBoardByPlayerId from "./getBoardByPlayerId";
import listItems from "./listItems";

export default async function generateNewBoard(playerId: string): Promise<Board | undefined> {
  // Make sure that the player does not already have a board
  const boards = await getBoardByPlayerId(playerId);

  if(boards) {
    console.log("Player already has a board");
    return undefined;
  }

  // Get a list of the current items
  let items = await listItems();

  // We don't have enough items
  if (!items || items.length < 9) {
    console.log("There are not enough items in the database");
    return undefined;
  }

  let length = items.length;
  let boxes: String[] = [];
  while(length--) {
    const randomPosition = Math.floor(Math.random() * (length + 1));
    const item = items.splice(randomPosition, 1)[0] as Item;
    if (item.id) {
      boxes.push(item.id);
    }
  }

  const boxJson = JSON.stringify(boxes);

  const board = {
    boxes: boxJson,
    playerId: playerId,
    marked: ""
  }

  console.log(board);

  return createBoard(board);

}