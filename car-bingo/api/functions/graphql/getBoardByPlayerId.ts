import { DynamoDB } from "aws-sdk";
import Board from "./BoardTable";

const dynamoDb = new DynamoDB.DocumentClient();

export default async function getBoardByPlayerId(
    playerId: String
): Promise<Board | undefined> {
    const params = {
        TableName: process.env.BOARD_TABLE as string,
        FilterExpression: 'playerId = :playerId',
        ExpressionAttributeValues: {':playerId' : playerId}
    };

    // we want to make playerId a secondary index so we don't have to scan the entire table
    const data = await dynamoDb.scan(params).promise();

    if ( data.Items && data.Items.length > 0 ) {
        return data.Items[0] as Board;
    }

    return undefined;
}