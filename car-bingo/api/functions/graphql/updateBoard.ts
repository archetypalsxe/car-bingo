import { DynamoDB } from "aws-sdk";
import Board from "./BoardTable";
import { getNewTimeToLive } from "./createBoard";

const dynamoDb = new DynamoDB.DocumentClient();

export default async function updateBoard(board: Board): Promise<Board> {
  board.timeToLive = getNewTimeToLive();

  console.log(`Updating a board.... ${JSON.stringify(board)}`);

  let updateFields = ['timeToLive = :timeToLive'];

  if (board.boxes) {
    updateFields.push('boxes = :boxes');
  }

  if (board.marked) {
    updateFields.push('marked = :marked');
  }

  if (board.playerId) {
    updateFields.push('playerId = :playerId');
  }

  const params = {
      Key: { id: board.id },
      ReturnValues: "ALL_NEW",
      UpdateExpression: `SET ${updateFields.join(", ")}`,
      TableName: process.env.BOARD_TABLE as string,
      ExpressionAttributeValues: {
        ":timeToLive": board.timeToLive,
        ":boxes": board.boxes,
        ":marked": board.marked,
        ":playerId": board.playerId,
      },
  };

  const updatedBoard = await dynamoDb.update(params).promise();
  return updatedBoard['Attributes'] as Board;
}