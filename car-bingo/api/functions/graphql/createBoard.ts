import { DynamoDB } from "aws-sdk";
import Board from "./BoardTable";
import listBoards from "./listBoards";

const dynamoDb = new DynamoDB.DocumentClient();

export default async function createBoard(board: Board): Promise<Board> {

    const existingBoards = await listBoards();
    if (existingBoards) {
        board.id = String(existingBoards.length + 1);
    } else {
        board.id = "1";
    }

    board.timeToLive = getNewTimeToLive();

    console.log(`Creating a board.... ${JSON.stringify(board)}`);

    const params = {
        Item: board as Record<string, unknown>,
        TableName: process.env.BOARD_TABLE as string,
    };

    await dynamoDb.put(params).promise();

    return board;
}

export function getNewTimeToLive(): number {
    let ttlDate = new Date();
    ttlDate.setHours(ttlDate.getHours() + 12);

    console.log(`Time to Live: ${ttlDate}`);

    // Convert from milliseconds to seconds
    return Math.floor(ttlDate.getTime() / 1000);
}