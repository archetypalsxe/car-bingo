import { CognitoJwtVerifier } from 'cognito-jwt-verify';

async function getPlayerId(context: any) : Promise<string | undefined> {
  if(context.headers.authorization) {
    const verifier = new CognitoJwtVerifier(
      process.env.AWS_REGION || "",
      process.env.USER_POOL_ID || "",
      process.env.USER_POOL_CLIENT_ID || "",
      false
    );
    try {
      const result = await verifier.verify(context.headers.id_token) as any;
      return result.sub || undefined;
    } catch (error) {
      console.log("Error validating token");
      console.log(error);
    }
  }

  return undefined;
}

export default async function checkPlayerId(context: any, playerId: string) : Promise<boolean> {
  const tokenPlayerId = await getPlayerId(context);

  if(tokenPlayerId) {
    if(tokenPlayerId != playerId) {
      console.error(`Mismatching player IDs: ${tokenPlayerId} and ${playerId}!`);
      throw new Error("Player ID does not match");
    }
  } else {
    // If it's a user that's not logged in, they shouldn't have any letters in their ID
    if(isNaN(parseFloat(playerId))) {
      console.error(`Unauthenticated user trying to impersonate ${playerId}`);
      throw new Error("Unauthenticated user");
    }
  }

  return true;
}
