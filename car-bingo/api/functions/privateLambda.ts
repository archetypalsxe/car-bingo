import { ApolloServer } from "apollo-server-lambda";
const { merge } = require('lodash');

import { typeDefs as privateTypeDefs } from "./privateTypeDefs"
import { private_resolvers } from "./privateResolvers"
import { typeDefs as publicTypeDefs} from "./publicTypeDefs"
import { public_resolvers  } from "./publicResolvers"

const typeDefs = `
  type Query {
    _empty: String
  }
  type Mutation {
    _empty: String
  }
`;

const resolvers = {
    Query: {
    },
    Mutation:{
    },
};

const server = new ApolloServer({
  typeDefs:[publicTypeDefs, privateTypeDefs],
  resolvers:merge(resolvers, public_resolvers, private_resolvers),
  introspection: !!process.env.IS_LOCAL,
  context: ({ event, context, express }) => ({
    headers: event.headers,
    functionName: context.functionName,
    event,
    context,
    expressRequest: express.req,
  }),
});

export const handler = server.createHandler();