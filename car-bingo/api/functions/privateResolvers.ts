import Board from "./graphql/BoardTable";
import Item from "./graphql/ItemTable";

import { public_queries, public_mutations } from "./publicResolvers"

import createItem from "./graphql/createItem";
import deleteItem from "./graphql/deleteItem";
import getBoard from "./graphql/getBoard";
import listBoards from "./graphql/listBoards";
import listItems from "./graphql/listItems";
import populateTables from "./graphql/populateTables";
import updateBoard from "./graphql/updateBoard";

const private_queries = {
  getBoard: (
    parent: undefined,
    args: { boardId: string }
  ) => getBoard(args.boardId),
  listBoards: () => listBoards(),
  listItems: () => listItems(),
};

const private_mutations = {
  createItem: (
    parent: undefined,
    args: { item: Item },
  ) => createItem(args.item),
  deleteItem: (
    parent: undefined,
    args: { item: Item },
  ) => deleteItem(args.item.id || ""),
  populateTables: (
    parent: undefined,
  ) => populateTables(),
  updateBoard: (
    parent: undefined,
    args: { board: Board },
  ) => updateBoard(args.board)
}

export const private_resolvers = {
  Query: {
    ...public_queries,
    ...private_queries,
  },
  Mutation: {
    ...public_mutations,
    ...private_mutations,
  },
};