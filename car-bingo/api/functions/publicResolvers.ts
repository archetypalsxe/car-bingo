import Board from "./graphql/BoardTable";

import checkPlayerId from "./authHelpers"

import createBoard from "./graphql/createBoard";
import deleteBoard from "./graphql/deleteBoard";
import getBoard from "./graphql/getBoard";
import getItem from "./graphql/getItem";
import generateNewBoard from "./graphql/generateNewBoard";
import getBoardByPlayerId from "./graphql/getBoardByPlayerId";
import updateBoardMarked from "./graphql/updateBoardMarked";

export const public_queries = {
  hello: async (parent: undefined, args: {}, context: any) => {
    "Hello, World!"
  },
  getBoardByPlayerId: async (
    parent: undefined,
    args: { playerId: string },
    context: any,
  ) => {
    await checkPlayerId(context, args.playerId);
    return getBoardByPlayerId(args.playerId);
  },
  getItem: (
    parent: undefined,
    args: { itemId: string }
  ) => getItem(args.itemId),
};

export const public_mutations = {
  createBoard: (
    parent: undefined,
    args: { board: Board },
  ) => createBoard(args.board),
  deleteBoard: async (
    parent: undefined,
    args: { boardId: string },
    context: any,
  ) => {
    const board = await getBoard(args.boardId);
    if (!board) {
      throw new Error("Invalid board");
    }
    await checkPlayerId(context, `${board.playerId}`);
    return deleteBoard(args.boardId);
  },
  generateNewBoard: async (
    parent: undefined,
    args: { playerId: string },
    context: any,
  ) => {
    await checkPlayerId(context, args.playerId);
    return generateNewBoard(args.playerId);
  },
  updateBoardMarked: async (
    parent: undefined,
    args: { boardId: string, markedBoxes: string },
    context: any,
  ) => {
    const board = await getBoard(args.boardId);
    if (!board) {
      throw new Error("Invalid board");
    }
    await checkPlayerId(context, `${board.playerId}`);
    return updateBoardMarked(args.boardId, args.markedBoxes);
  },
};

export const public_resolvers = {
  Query: {
    ...public_queries
  },
  Mutation: {
    ...public_mutations
  },
};