import { gql  } from "apollo-server-lambda";

export const typeDefs = gql`
  type Board {
    id: ID!
    playerId: String!,
    boxes: String!,
    marked: String
  }

  type Item {
    id: ID!
    name: String!
    imageUrl: String
  }

  input UpdateBoardInput {
    id: String!,
    playerId: String,
    boxes: String,
    marked: String
  }

  input CreateItemInput {
    name: String!
    imageUrl: String
  }

  input DeleteItemInput {
    id: ID!
  }

  type Query {
    getBoard(boardId:String): Board
    listBoards: [Board]
    listItems: [Item]
  }

  type Mutation {
    createItem(item:CreateItemInput!):Item,
    deleteItem(item:DeleteItemInput!):String,
    populateTables:String,
    updateBoard(board:UpdateBoardInput):Board,
  }
`;