import {
  Api,
  Auth,
  Bucket,
  GraphQLApi,
  ReactStaticSite,
  StackContext,
  Table,
  toCdkDuration
} from "@serverless-stack/resources";

import { RemovalPolicy } from "aws-cdk-lib";
import { AccountRecovery, Mfa, UserPool } from "aws-cdk-lib/aws-cognito";
import * as iam from "aws-cdk-lib/aws-iam";
import * as secretsmanager from "aws-cdk-lib/aws-secretsmanager"
import * as cdk from "aws-cdk-lib"

export function MyStack({ stack, app }: StackContext) {
  const stage = app.stage;
  const domain_name = process.env.DOMAIN_NAME;
  const project_name = process.env.PROJECT_NAME;
  const region = app.region;
  const aws_account = stack.account;

  const sub_domain = (stage == "prod") ? project_name : `${project_name}-${stage}`;
  // This should just be for the local environment
  const api_sub_domain = (stage == "local") ? `${project_name}-api` : `${project_name}-api-${stage}`;

  if (stage == "dev" || stage == "prod") {
    const pipeline_user_name = `${project_name}-${stage}-cicd`;
    const secret_id_name = `${project_name}-${stage}-cicd-id`;
    const secret_key_name = `${project_name}-${stage}-cicd-key`;

    const pipeline_policy = new iam.ManagedPolicy(stack, 'PipelineUserPolicy', {
      managedPolicyName: `${project_name}-${stage}-cicd-policy`,
      statements: [
        new iam.PolicyStatement({
          sid: 'CloudFormation',
          actions: [
            "cloudformation:*",
          ],
          resources: [
            `arn:aws:cloudformation:${region}:${aws_account}:stack/${stage}-${project_name}-*`,
          ],
          conditions: {
            'ForAllValues:StringEquals': {
              'aws:ResourceTag/sst:stage': `${stage}`,
              'aws:ResourceTag/sst:app': `${project_name}`,
            },
          },
        }),
        new iam.PolicyStatement({
          sid: 'ManageApi',
          actions: [
            "iam:*",
            "logs:*",
            "dynamodb:*",
            "apigateway:*",
            "lambda:*",
          ],
          resources: [
            `arn:aws:iam::${aws_account}:role/${stage}-${project_name}-`,
            `arn:aws:logs:${region}:${aws_account}:log-group:*${stage}-${project_name}-*`,
            `arn:aws:dynamodb:${region}:${aws_account}:table/${stage}-${project_name}-*`,
            // Wish that we could limit further, but the rest API ID does not seem to be predictable
            `arn:aws:apigateway:${region}::/restapis*`,
            `arn:aws:lambda:${region}:${aws_account}:function:${stage}-${project_name}-*`,
          ],
          conditions: {
            'ForAllValues:StringEquals': {
              'aws:ResourceTag/sst:stage': `${stage}`,
              'aws:ResourceTag/sst:app': `${project_name}`,
            },
          },
        }),
        new iam.PolicyStatement({
          sid: "ManageCustomDomains",
          actions: [
            "acm:*",
            "route53:*",
            "cloudfront:*",
          ],
          resources: [
            // These all don't seem like they can be further restricted
            `arn:aws:acm:${region}:${aws_account}:certificate/*`,
            `arn:aws:route53:::hostedzone/*`,
            `arn:aws:cloudfront::${aws_account}:distribution/*`,
          ],
          conditions: {
            'ForAllValues:StringEquals': {
              'aws:ResourceTag/sst:stage': `${stage}`,
              'aws:ResourceTag/sst:app': `${project_name}`,
            },
          },
        }),
        new iam.PolicyStatement({
          sid: "CDKUse",
          actions: [
            "ssm:GetParameter",
            "s3:*",
            "iam:PassRole",
          ],
          resources: [
            `*`,
          ],
          conditions: {
            'ForAllValues:StringEquals': {
              'aws:ResourceTag/aws:cloudformation:stack-name': "CDKToolkit",
            },
          },
        }),
        new iam.PolicyStatement({
          sid: "CDKAssumeRole",
          actions: [
            "sts:AssumeRole",
          ],
          resources: [
            `*`,
          ],
          conditions: {
            'ForAllValues:StringEquals': {
              'aws:ResourceTag/aws-cdk:bootstrap-role': "lookup",
            },
          },
        }),
        new iam.PolicyStatement({
          sid: "StarAccess",
          actions: [
            "cloudformation:CreateChangeSet",
            "cloudformation:DeleteChangeSet",
            "cloudformation:DescribeChangeSet",
            "cloudformation:DescribeStacks",
            "cloudformation:ExecuteChangeSet",
            "cloudformation:GetTemplate",
            "route53:ListHostedZonesByName",
          ],
          resources: [
            "*",
          ],
        }),
      ],
    });

    const pipeline_user = new iam.User(stack, `PipelineUser`, {
      userName: pipeline_user_name,
      managedPolicies: [ pipeline_policy ],
    });

    const accessKey = new iam.AccessKey(stack, 'AccessKey', {  user: pipeline_user });
    const accessKeyId = new cdk.SecretValue(accessKey.accessKeyId.toString());
    new secretsmanager.Secret(stack, `${project_name}-cicd-access-id`, {
      secretName: secret_id_name,
      secretStringValue: accessKeyId,
    });
    const accessKeyValue = new cdk.SecretValue(accessKey.secretAccessKey.toString());
    new secretsmanager.Secret(stack, `${project_name}-cicd-access-key`, {
      secretName: secret_key_name,
      secretStringValue: accessKeyValue,
    });
  }

  const s3Bucket = new Bucket(stack, "ItemImages", {
    cdk: {
      bucket: {
        autoDeleteObjects: true,
        removalPolicy: RemovalPolicy.DESTROY,
      }
    }
  });


  const boardTable = new Table(stack, "Board", {
    fields: {
      id: "string",
      playerId: "string",
      boxes: "string",
      marked: "string",
    },
    primaryIndex: { partitionKey: "id" },
    timeToLiveAttribute: "timeToLive",
    globalIndexes: {
      playerId: { partitionKey: "playerId" },
    },
    // Destroy the table if we remove the stack
    cdk: {
      table: {
        removalPolicy: RemovalPolicy.DESTROY,
      }
    },
  });

  const itemsTable = new Table(stack, "Items", {
    fields: {
      id: "string",
      name: "string",
      imageUrl: "string",
    },
    primaryIndex: { partitionKey: "id" },
    // Destroy the table if we remove the stack
    cdk: {
      table: {
        removalPolicy: RemovalPolicy.DESTROY,
      }
    },
  });

  const auth = new Auth(stack, "Auth", {
    login: ["email"],
    cdk: {
      userPool: {
        removalPolicy: RemovalPolicy.DESTROY,
        accountRecovery: AccountRecovery.EMAIL_ONLY,
        mfa: Mfa.REQUIRED,
        mfaSecondFactor: {
          sms: false,
          otp: true,
        },
        userVerification: {
          emailSubject: "Please verify your new Bingo account!",
        },
      }
    },
  });

  // Not able to directly add permissions to the auth object, so have to manually
  // add these permissions
  const admin_api_role = new iam.Role(stack, "AdminApiRole", {
    assumedBy: new iam.ServicePrincipal("lambda.amazonaws.com"),
    managedPolicies: [
      {
        managedPolicyArn: "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole",
      },
      {
        managedPolicyArn: "arn:aws:iam::aws:policy/AmazonCognitoPowerUser",
      },
      {
        managedPolicyArn: "arn:aws:iam::aws:policy/AWSXRayDaemonWriteAccess",
      },
    ],
    inlinePolicies: {
      // [sic]
    },
  });

  // API for Managing User Pools
  const admin_api = new Api(stack, "AdminApi", {
    defaults: {
      authorizer: "jwt",
      function: {
        role: admin_api_role,
        srcPath: "admin_api",
        environment: {
          userPoolId: auth.userPoolId,
        },
      }
    },

    authorizers: {
      // IMPORTANT: This field (jwt) has to match the authorizer used in the defaults
      jwt: {
        type: "user_pool",
        userPool: {
          id: auth.userPoolId,
          clientIds: [auth.userPoolClientId],
        },
      },
    },

    routes: {
      "POST /userpool-delete": {
        function: "userpool.delete_user",
        authorizer: "none",
      },
      "GET /userpool": {
        function: "userpool.get",
      },
    },
  });


  const public_api = new GraphQLApi(stack, "PublicApolloApi", {
    // Only use a custom domain for the local environment
    customDomain:
      stage == "local" ? {
        domainName: `${api_sub_domain}.${domain_name}`,
        hostedZone: `${domain_name}`,
      } : undefined,

    defaults: {
      function: {
        permissions: [boardTable, itemsTable],
        environment: {
          BOARD_TABLE: boardTable.tableName,
          ITEMS_TABLE: itemsTable.tableName,
          USER_POOL_ID: auth.userPoolId,
          IDENTITY_POOL_ID: auth.cognitoIdentityPoolId ? auth.cognitoIdentityPoolId.toString() : "",
          USER_POOL_CLIENT_ID: auth.userPoolClientId,
        }
      }
    },

    server: {
      handler: "functions/publicLambda.handler",
      bundle: {
        format: "cjs",
      },
    },
  });

  // No point for a custom domain since you have to be authorized to access this
  const private_api = new GraphQLApi(stack, "PrivateApolloApi", {
    authorizers: {
      // IMPORTANT: This field (jwt) has to match the authorizer used in the defaults
      jwt: {
        type: "user_pool",
        userPool: {
          id: auth.userPoolId,
          clientIds: [auth.userPoolClientId],
        },
      },
    },

    defaults: {
      authorizer: "jwt",
      function: {
        permissions: [boardTable, itemsTable],
        environment: {
          BOARD_TABLE: boardTable.tableName,
          ITEMS_TABLE: itemsTable.tableName,
          USER_POOL_ID: auth.userPoolId,
          IDENTITY_POOL_ID: auth.cognitoIdentityPoolId ? auth.cognitoIdentityPoolId.toString() : "",
          USER_POOL_CLIENT_ID: auth.userPoolClientId,
        }
      }
    },

    server: {
      handler: "functions/privateLambda.handler",
      bundle: {
        format: "cjs",
      },
    },
  });

  auth.attachPermissionsForAuthUsers([public_api, private_api, admin_api]);

  const frontEnd = new ReactStaticSite(stack, "ReactSite", {
    path: "frontend",
    environment: {
      REACT_APP_STAGE: stage,
      REACT_APP_PUBLIC_API_URL: public_api.url,
      REACT_APP_PRIVATE_API_URL: private_api.url,
      REACT_APP_ADMIN_API_URL: admin_api.url,
      REACT_APP_ITEM_BUCKET: s3Bucket.bucketName,
      REACT_APP_USER_POOL_ID: auth.userPoolId,
      REACT_APP_IDENTITY_POOL_ID: auth.cognitoIdentityPoolId ? auth.cognitoIdentityPoolId.toString() : "",
      REACT_APP_USER_POOL_CLIENT_ID: auth.userPoolClientId,
      REACT_APP_DOMAIN: `${sub_domain}.${domain_name}`,
      REACT_APP_SUB_DOMAIN: `${sub_domain}`,
    },
    customDomain: {
      domainName: `${sub_domain}.${domain_name}`,
      hostedZone: `${domain_name}`,
    },
  });

  // Show the API endpoint in output
  stack.addOutputs({
    AdminApiEndpoint: admin_api.url,
    PublicApiEndpoint: public_api.url,
    PrivateApiEndpoint: private_api.url,
    FrontEnd: frontEnd.url,
  });
}