import { render, screen } from '@testing-library/react';
import App from './App';
import { MockedProvider } from "@apollo/client/testing";
import { Amplify, Auth } from "aws-amplify";
import { act } from 'react-dom/test-utils';
import { BrowserRouter as Router } from "react-router-dom";

test('Render the app component', async () => {
  const mocks = [];

  Amplify.configure({
    Auth: {
      mandatorySignIn: true,
      region: process.env.REACT_APP_REGION,
      userPoolId: process.env.REACT_APP_USER_POOL_ID,
      identityPoolId: process.env.REACT_APP_IDENTITY_POOL_ID,
      userPoolWebClientId: process.env.REACT_APP_USER_POOL_CLIENT_ID,
    },
    API: {
      endpoints: [
        {
          name: "userpool",
          region: process.env.REACT_APP_REGION,
          endpoint: process.env.REACT_APP_ADMIN_API_URL,
        },
      ],
    },
  });


  jest.spyOn(Auth, 'currentSession').mockImplementation(() => ({
    getAccessToken: () => ({
      getJwtToken: () => 'mockAccessToken'
    }),
  }));

  jest.spyOn(Auth, 'currentUserInfo').mockImplementation(() => ({
    username: 'mock name',
    attributes: { email: 'mock@email.com' },
  }));

  await act(async () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Router>
          <App />
        </Router>
      </MockedProvider>
    );
  });

  const linkElement = screen.getByText(/Player: mock@email.com/i);
  expect(linkElement).toBeInTheDocument();
});
