import { APIGatewayProxyResult } from "aws-lambda";
import { CognitoIdentityServiceProvider } from "aws-sdk";

export async function delete_user(event): Promise<APIGatewayProxyResult> {
  const parameters = JSON.parse(event.body);

  var params = {
    UserPoolId: process.env.userPoolId,
    Username: parameters.username
  };

  try {
    const response = await deleteUser(params);
    return {
      statusCode: 200,
      body: JSON.stringify("Deletion was successful!"),
    };
  } catch (exception) {
    return {
      statusCode: 500,
      body: JSON.stringify(exception.message)
    };
  }
}

export async function get(): Promise<APIGatewayProxyResult> {
  return {
    statusCode: 200,
    body: JSON.stringify("This is a get response!"),
  };
}

const deleteUser = function(params) {
  return new Promise((resolve, reject) => {
    const cognito = new CognitoIdentityServiceProvider();
    cognito.adminDeleteUser(params, function(error, data) {
      if (error) return reject(error);

      resolve(data);
    });
  });
}