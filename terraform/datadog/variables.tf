variable "region" {
  description = "The deploy target region in AWS"
  type = string
}

variable "project_name" {
  description = "Identifier for this project"
  type = string
}

variable "unique_identifiers" {
  type = list(string)
  description = "A list of objects that contain identifying information for each environment"
}

variable "aws_profile" {
  description = "The AWS profile we should be using to interact with Terraform"
  type        = string
}

variable "alert_emails" {
  description = "List of emails to receive datadog alerts from"
  type = list(string)
  default = []
}

variable "invocations_threshold_critical" {
  type = list(object({
    environment = string
    threshold = number
  }))
  description = "Alerting threshold in number of invocations"
}

variable "invocations_threshold_warning" {
  type = list(object({
    environment = string
    threshold = number
  }))
  description = "Alerting threshold in number of invocations"
}

variable "dynamodb_size_threshold" {
  type = list(object({
    environment = string
    threshold = number
  }))
  description = "Alerting threshold for the number of records in DynamoDB tables"
}