locals {
  config = yamldecode(file("terragrunt-config.yaml"))
  # TODO Make this dynamic
  dev_config = local.config.environments.dev
  prod_config = local.config.environments.prod
}

inputs = {
  project_name = local.config.project_name
  unique_identifiers = [
    local.dev_config.unique_identifier,
    local.prod_config.unique_identifier,
  ]
  region = local.config.aws_region
  aws_profile = local.config.aws_profile
  alert_emails = local.config.alert_emails
  invocations_threshold_critical = [
    { environment: "dev", threshold: local.dev_config.invocations_threshold_critical },
    { environment: "prod", threshold: local.prod_config.invocations_threshold_critical },
  ]
  invocations_threshold_warning = [
    { environment: "dev", threshold: local.dev_config.invocations_threshold_warning },
    { environment: "prod", threshold: local.prod_config.invocations_threshold_warning },
  ]
  dynamodb_size_threshold = [
    { environment: "dev", threshold: local.dev_config.dynamodb_size_threshold },
    { environment: "prod", threshold: local.prod_config.dynamodb_size_threshold },
  ]
  working_dir = get_terragrunt_dir()
}

dependencies {
  paths = []
}

terraform {
  extra_arguments "env" {
    commands = ["init", "apply", "refresh", "import", "plan", "taint", "untaint"]
  }

  extra_arguments "init_args" {
    commands = [
      "init"
    ]

    arguments = [
      "--reconfigure",
    ]
  }
}

remote_state {
  backend = "s3"

  config = {
    region = local.config.aws_region

    # TODO Not using a unique bucket and table per environment
    bucket  = "${local.config.project_name}-remote-state"
    key     = "terraform.tfstate"
    encrypt = true

    dynamodb_table = "${local.config.project_name}-remote-locks"
  }
}