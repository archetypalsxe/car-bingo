terraform {
  backend "s3" {}
  required_providers {
    datadog = {
      source = "DataDog/datadog"
    }
  }
}

provider "aws" {
  profile = var.aws_profile
  region  = var.region
  default_tags {
    tags = {
      ProjectName = var.project_name
      Owner = "terraform"
    }
  }
}

data "aws_secretsmanager_secret" "datadog_key_secrets" {
  name = "datadog-keys-${var.project_name}"
}

data "aws_secretsmanager_secret_version" "datadog_key_secrets" {
  secret_id = data.aws_secretsmanager_secret.datadog_key_secrets.id
}

provider "datadog" {
  api_key = jsondecode(data.aws_secretsmanager_secret_version.datadog_key_secrets.secret_string)["api_key"]
  app_key = jsondecode(data.aws_secretsmanager_secret_version.datadog_key_secrets.secret_string)["app_key"]
}

module "lambdaDashboards" {
  source = "git@gitlab.com:archetypalsxe/datadog-terraform-resources.git//modules/lambda_dashboard"
  for_each = toset(var.unique_identifiers)
  # Not able to have dynamic provider names
  providers = {
    datadog = datadog
  }
  project_name = var.project_name 
  unique_identifier = each.key
  filter_tags = [
    "sst_stage:${each.key}",
    "sst_app:${var.project_name}"
  ]
  alert_emails = var.alert_emails
  invocations_threshold_critical = var.invocations_threshold_critical[index(var.invocations_threshold_critical.*.environment, each.key)].threshold
  invocations_threshold_warning = var.invocations_threshold_warning[index(var.invocations_threshold_warning.*.environment, each.key)].threshold
  dynamodb_table = "${each.key}-${var.project_name}-board"
  dynamodb_size_threshold = var.dynamodb_size_threshold[index(var.dynamodb_size_threshold.*.environment, each.key)].threshold
}